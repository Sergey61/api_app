class ApiController < ApplicationController
  def index
  end

  def filter
  	data=[
  		{id: 1, name: "Oleg", age: 22},
  		{id: 2, name: "Ivan", age: 23},
  		{id: 3, name: "Paul", age: 24}
  	]
  	if params[:name]
  		filtred_data = data.select { |i| i[:name].include?(params[:name]) }	
  		render text: filtred_data.to_json
  	else
  		render text: data.to_json
  	end
  end
end
