app.controller('AjaxCtrl', ['$scope', '$http', function($scope, $http){
	$scope.$watch('search', function(val){
		$http({method:"GET", url:"/api/filter", params: {name: val}}).success(function (res) {
			$scope.find = res;
		})
	})
}])